import http from 'http';
import Config from './config/config.json';
import { User, Product } from './models';

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end(Config.name + '\n');
  console.log(Config.name);

  const user = new User();
  const product = new Product();

}).listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');