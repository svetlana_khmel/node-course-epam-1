
# NODE.JS MODULES. NPM # 

### Installing Node.js & Configuring Babel with Nodemon ###
* Install Node.js (choose any option that suits you nvm, brew, apt, .exe, etc).
* Create a directory for the future project.
 - Use npm to set author name and email for npm config:
 - Use npm -l to discover available npm commands and full usage info.
* Initialize package.json using npm command.
* Create the main application file app.js. This file will be executed on npm start.
* Install the following npm packages as devDependencies:
 - Babel core
 - Babel preset env
 - Babel preset stage 2
 - Babel register
 - Nodemon
 
* After the installation all these packages should be saved and listed in package.json.
* Configure scripts section in package.json to include:
 - start script which should compile app.js using babel and run it in nodemon.
 - test script to run application tests that we will create in the future (for now it could be empty).

### Adding Modules ### 

* Create config directory inside your project.
* Create json module in config directory to store configs of application. For now add just one field name that stores the name of the app: “Node.js Homework Application”.
* Create models directory.
* Create User.js module in models directory. It should implement and export class User (use ECMAScript 2015) with a constructor that logs “User module” to console.
* Create Product.js module in models directory which exports Product class with a constructor that logs “Product module” to console.

* In the main application file import json module defined in config directory (use ECMAScript 2015 as well instead of require) and log the name of application to console.
* In the main application file import modules defined in models directory. There should be one import command that brings all our models to the app.
* Create instances of User and Product classes. Appropriate messages should be logged to console.

## Evaluation Criteria ##

* Nothing has been done except the project’s structure.
* package.json has been created and contains the list of required packages.
* All three modules have been created and the classes have been implemented.
* The modules are imported to the main module as described in task 7 and 8.
* package.json “start” script uses babel and nodemon to run the app.